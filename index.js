
const express = require('express')
const cors = require('cors')
const bcrypt = require('bcrypt')
const app = express()
const PORT = 4000
const util = require('util');
const { validator, body } = require('express-validator');
const { check, oneOf, validationResult } = require('express-validator/check');
app.use(express.json())
app.use(cors())


const API = `/api`;
const AUTH = `${API}/auth`;
const LOGIN = `${AUTH}/login`;
const REGISTER = `${API}/register`;

const REGISTER_SCHEMA = [body('email').exists().withMessage('email is required').isEmail().withMessage('email not valid'), body('password').exists().withMessage('password is required').isStrongPassword().withMessage('password is week'), body('name').exists().withMessage('email is required').isLength({ min: 5 }).withMessage('wrong email length')];


const users = []
app.get('/api/users', (req, res) => {
  res.json(users)
})

app.post(REGISTER,
  REGISTER_SCHEMA,
  async (req, res) => {

    const { email, name, password, repPassword } = req.body
    const user = { email, password, name }
    if (password === repPassword) res.send('Passwords needs to be same')
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      res.status(500).send({ errors: errors.array() })
    }
    users.push(user)
    res.status(201).send()
  })

app.post(LOGIN, async (req, res) => {
  const user = users.find(user => user.email === req.body.email)
  if (user == null) {
    return res.status(400).send('Cannot find user')
  }
  try {
    if (await bcrypt.compare(req.body.password, user.password)) {
      res.send('Success')
    } else {
      res.send('Not Allowed')
    }
  } catch {
    res.status(500).send()
  }
})

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`)
})