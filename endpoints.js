export const API = `/api`;
export const REGISTER = `${API}/register`;
export const AUTH = `${API}/auth`;
export const LOGIN = `${AUTH}/login`;
